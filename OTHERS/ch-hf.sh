#!/bin/bash
#####################################################
# $id: ch-hf; ver-0.1 (c) 2003 USM Bish
# Change Header-Footers of OTHERS snippets
#####################################################

PROGNAME=$(basename $0)
VERSION=0.1

IN_DIR=/var/signature/OTHERS
OUTDIR=/var/signature/OTHERS-NEW
OLDDIR=/var/signature/OTHERS-OLD

TEMP_FILE1=/tmp/$PROGNAME.1
TEMP_FILE2=/tmp/$PROGNAME.2

#####################################################
# Replace the top and bottom lines as needed.  Ensure
# you escape out the [ and ] symbols,  otherwise they
# would be interpreted by the shell, and not taken as
# a literal ! 

# Original top line
TOP1='=====oooO==U==Oooo================\[Your Name\]==============='

# Change top line to:
TOP2='=====oooO==U==Oooo============================\[USM Bish\]===='
# TOP2='=====oooO==U==Oooo============================================'

# Original bottom line
BOT1='======================\[URL:http://your_home_page/~url\]======'

# Change bottom line to:
BOT2='=====================\[URL:http://geocities.com/usmbish/\]===='
# BOT2='=============================================================='

#####################################################
# Main script begins
#####################################################

echo
echo "$PROGNAME                        Version : $VERSION"
echo "[CHange Header Footer (of OTHERS category sigs)]"
echo

if ! [ -s $IN_DIR/001.others ]; then
     echo "ERR : $IN_DIR/001.others NOT found"
     exit 1
fi
     
if ! [ -d $OUTDIR ]; then
     mkdir -p $OUTDIR
fi

echo -en "Proceeding : "
for i in $(ls $IN_DIR/*.others); do
    NAYME=$(basename $i)
    cat $i | sed -e "s|$TOP1|$TOP2|g" > $TEMP_FILE1
    cat $TEMP_FILE1 | sed -e "s|$BOT1|$BOT2|g" > $TEMP_FILE2
    # Now copy this over to a new file in a NEW directory
    cat $TEMP_FILE2 > $OUTDIR/$NAYME
    echo -en "."
done
echo

# Now reset things by swapping the directories

mv $IN_DIR $OLDDIR
mv $OUTDIR $IN_DIR

echo
echo "Done ..."
echo "New files are in: $IN_DIR"
echo "Old files are in: $OLDDIR"

rm -f $TEMP_FILE1
rm -f $TEMP_FILE2

exit
         